#include <chrono>
#include <fstream>
#include <iostream>
#include <mpu6050/mpu6050.h>
#include <mqtt/async_client.h>
#include <nlohmann/json.hpp>
#include <string>
#include <thread>

int main(int argc, char** argv)
{
    using namespace std::chrono_literals;

    if(argc != 2)
    {
        std::cout << "Error: missing mqtt broker address" << std::endl;
        return -1;
    }

    const std::string mqttBrokerAddr{argv[1]};
    const std::string myId{"rpi1"}; //TODO fill from env variable
    const std::string mqttTopic{"rpi-app/anti-theft/acceleration"};
    const int mqttQos{1};

    std::cout << "creating client" << std::endl;
    mqtt::async_client client(mqttBrokerAddr, "");
    std::cout << "connecting to broker" << std::endl;
    client.connect()->wait();
    mqtt::topic topic(client, mqttTopic, mqttQos);

    CalibrationData accelerometerCalibration;
    try
    {
        std::ifstream jsonCalibrationFile("calibration.json");
        auto jsonData = nlohmann::json::parse(jsonCalibrationFile);
        accelerometerCalibration.gyro_offset_X = jsonData["gyroscope_offset_r"];
        accelerometerCalibration.gyro_offset_Y = jsonData["gyroscope_offset_p"];
        accelerometerCalibration.gyro_offset_Z = jsonData["gyroscope_offset_y"];
        accelerometerCalibration.accel_offset_X = jsonData["accelerometer_offset_x"];
        accelerometerCalibration.accel_offset_Y = jsonData["accelerometer_offset_y"];
        accelerometerCalibration.accel_offset_Z = jsonData["accelerometer_offset_z"];
    }
    catch(const std::exception& e)
    {
        std::cout << "exception during parsing of calibration json, using zeros, error: " << e.what() << std::endl;
        accelerometerCalibration = {};
    }
    accelerometerCalibration.gyro_range = 1;
    accelerometerCalibration.accel_range = 1;

    MPU6050 device(0x68, accelerometerCalibration);
    float ax, ay, az, gr, gp, gy;
    device.calc_yaw = true;

    while(true)
    {
        device.getAngle(0, &gr);
        device.getAngle(1, &gp);
        device.getAngle(2, &gy);
        std::cout << "Current angle around the roll axis: " << gr << "\n";
        std::cout << "Current angle around the pitch axis: " << gp << "\n";
        std::cout << "Current angle around the yaw axis: " << gy << "\n";
        device.getAccel(&ax, &ay, &az);
        device.getGyro(&gr, &gp, &gy);
        std::cout << "Accelerometer Readings: X: " << ax << ", Y: " << ay << ", Z: " << az << "\n";
        std::cout << "Gyroscope Readings: X: " << gr << ", Y: " << gp << ", Z: " << gy << "\n";
        double acceleration = ax * ax + ay * ay + az * az;
        acceleration = sqrt(acceleration);
        auto accelerationString = std::to_string(acceleration);
        std::cout << "Accelerometer total value = " << accelerationString << "\n";
        accelerationString = myId + " " + accelerationString;

        // send to mqtt
        topic.publish(accelerationString)->wait();
        std::this_thread::sleep_for(1s);
    }

    client.disconnect()->wait();

    return 0;
}
