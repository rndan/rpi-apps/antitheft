#include <fstream>
#include <mpu6050/mpu6050.h>
#include <nlohmann/json.hpp>
#include <string>

int main(int argc, char** argv)
{
    CalibrationData accelerometerCalibration;

    MPU6050 device(0x68, accelerometerCalibration);
    float ax, ay, az, gr, gp, gy;
    device.calc_yaw = true;

    std::cout << "Calculating the offsets. Please keep the accelerometer level and still. This could take a couple of minutes..."
              << std::endl;
    device.getOffsets(10000, &ax, &ay, &az, &gr, &gp, &gy);
    std::cout << "Gyroscope R,P,Y: " << gr << "," << gp << "," << gy << std::endl
              << "Accelerometer X,Y,Z: " << ax << "," << ay << "," << az << std::endl;

    nlohmann::json calibrationJson;
    calibrationJson["gyroscope_offset_r"] = gr;
    calibrationJson["gyroscope_offset_p"] = gp;
    calibrationJson["gyroscope_offset_y"] = gy;
    calibrationJson["accelerometer_offset_x"] = ax;
    calibrationJson["accelerometer_offset_y"] = ay;
    calibrationJson["accelerometer_offset_z"] = az;
    std::ofstream outJsonFile("calibration.json");
    outJsonFile << calibrationJson;

    return 0;
}
