FROM ubuntu

RUN apt update && apt install libssl-dev libi2c-dev -y

ENV LD_LIBRARY_PATH /libs

COPY install.paho.mqtt.cpp/usr/local/lib/libpaho-mqttpp3.so.1 /libs/
COPY install.paho.mqtt.cpp/usr/local/lib/libpaho-mqtt3as.so.1 /libs/
COPY build.antitheft/antiTheft /mybin/
COPY build.antitheft/libmpu6050.so /libs/
COPY build.antitheft/mpu6050Calibrator /mybin/

ENTRYPOINT [ "/mybin/antiTheft" ]
CMD [ "mqtt://172.16.200.20:1883" ]

